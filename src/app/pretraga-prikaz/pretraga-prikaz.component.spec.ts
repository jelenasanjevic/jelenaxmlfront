import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PretragaPrikazComponent } from './pretraga-prikaz.component';

describe('PretragaPrikazComponent', () => {
  let component: PretragaPrikazComponent;
  let fixture: ComponentFixture<PretragaPrikazComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PretragaPrikazComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PretragaPrikazComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
