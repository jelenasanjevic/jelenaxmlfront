import { Component, OnInit, ViewChild } from '@angular/core';
import { PretragaComponent } from '../pretraga/pretraga.component'
import {ServisiService} from '../servisi/servisi.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { resolveDefinition } from '@angular/core/src/view/util';
import { Router } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
// import ViewChild;

@Component({
  selector: 'app-pretraga-prikaz',
  templateUrl: './pretraga-prikaz.component.html',
  styleUrls: ['./pretraga-prikaz.component.scss']
})

export class PretragaPrikazComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  smestaj = [];

  displayedColumns = [ 'lokacija','kategorija','opis', 'ocena','izaberi','izaberi1'];
  dataSource = new MatTableDataSource<any>();

  constructor(private _data : ServisiService,private router: Router) { }

  ngOnInit() {
      this._data.pretrazeniSmestaj.subscribe(res => {
          this.smestaj = res;
          this.dataSource = new MatTableDataSource<any>(res);
          this.dataSource.sort = this.sort;
        }
      )
  }
  rezervisi(smestajId) {
    this._data.rezervacija(smestajId).subscribe(res => {
     
    });
    this.router.navigate(['/profil']);
   
  }
  vidiSlike(smestajId) {
    this._data.getSlikeForSmestaj(smestajId).subscribe(res => {
      this._data.slikeUpdate(res);
    })
    this.router.navigate(['/slike','1']);
    //  this.router.navigate(['/slike', smestajId]);
  }

  vidiOcene(smestajId) {
    this._data.vidiOcene(smestajId).subscribe(res => {
      this._data.oceneUpdate(res);
    })
    this.router.navigate(['/vidiOcene']);
  }

  vidiKomentare(smestajId) {
    this._data.vidiKomentare(smestajId).subscribe(res => {
      this._data.komentariUpdate(res);    
    })
    
    this.router.navigate(['/vidiKomentare']);

  }

}

