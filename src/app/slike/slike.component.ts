import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import {  ActivatedRoute } from '@angular/router';
import {ServisiService} from '../servisi/servisi.service';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-slike',
  templateUrl: './slike.component.html',
  styleUrls: ['./slike.component.scss']
})
export class SlikeComponent implements OnInit {

  constructor(private _data : ServisiService,private router: Router,private route: ActivatedRoute) { }

  korisnik:any;
  nizSlika:any;
  id:any;
  smestaj=[];

  ngOnInit() {

    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    
    this._data.getSlikeForSmestaj(this.id).subscribe(data=>{
      console.log(data);
      this.nizSlika = data;
    });
  }

}
