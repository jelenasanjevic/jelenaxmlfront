import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ServisiService} from './servisi/servisi.service';
import {HttpModule} from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MaterialModule } from './modules/material/material.module';

import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TokenInterceptor } from './services/token.interceptor';
import { EmailDialogComponent } from './components/email-dialog/email-dialog.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

import { AppComponent } from './app.component';
import { PretragaComponent } from './pretraga/pretraga.component';
import { AppNavbarComponent } from './app-navbar/app-navbar.component';
import { PretragaPrikazComponent } from './pretraga-prikaz/pretraga-prikaz.component';
import { PorukeComponent } from './poruke/poruke.component';
import { PregledComponent } from './pregled/pregled.component';


import { RouterModule, Routes } from '@angular/router';
//import { routingComponents } from './app-routing.module';


import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
  MAT_DIALOG_DEFAULT_OPTIONS
} from '@angular/material';
import { PosaljiPorukuComponent } from './posalji-poruku/posalji-poruku.component';
import { VidiKomentareComponent } from './vidi-komentare/vidi-komentare.component';
import { VidiOceneComponent } from './vidi-ocene/vidi-ocene.component';
import { SlikeComponent } from './slike/slike.component';
import { ProfilComponent } from './profil/profil.component';
import { AuthService } from './services/auth.service';
import { NovaPorukaComponent } from './nova-poruka/nova-poruka.component';


@NgModule({
  declarations: [
    AppComponent,
    PretragaComponent,
    AppNavbarComponent,
    PretragaPrikazComponent,
    PorukeComponent,
    PregledComponent,
    PosaljiPorukuComponent,
    VidiKomentareComponent,
    VidiOceneComponent,
    SlikeComponent,
    ProfilComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    ResetPasswordComponent,
    NavbarComponent,
    EmailDialogComponent,
    ChangePasswordComponent,
    NovaPorukaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    BrowserAnimationsModule,
    MatStepperModule,
    RouterModule,
    CommonModule,
    MaterialModule
    
  ],
  providers: [ServisiService,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent],
  entryComponents: [
    EmailDialogComponent
  ]
})
export class AppModule { }
