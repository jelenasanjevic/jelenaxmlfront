import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
import {ServisiService} from '../servisi/servisi.service';

@Component({
  selector: 'app-pretraga',
  templateUrl: './pretraga.component.html',
  styleUrls: ['./pretraga.component.scss']
})
export class PretragaComponent implements OnInit {

  prikaziDetaljnuPretragu = false;
  // myForm: FormGroup;

  constructor(private registrationService : ServisiService,private router: Router) { }

  PretragaForm : FormGroup;

  ngOnInit() {
    this.PretragaForm = new FormGroup({
      lokacija: new FormControl('',[Validators.required]),
      datumOd: new FormControl('',[Validators.required]),
      datumDo: new FormControl('',[Validators.required]),
      brojOsoba: new FormControl('',[Validators.required]),
      tipSmestaja: new FormControl('',[Validators.required]),
      kategorijaSmestaja: new FormControl('',[Validators.required]),
      parking: new FormControl('',[]),
      wifi: new FormControl('',[]),
      dorucak: new FormControl('',[]),
      polupansion: new FormControl('',[]),
      tv: new FormControl('',[]),
      mini_kuhinja: new FormControl('',[]),
      privatno_kupatilo: new FormControl('',[])
    })
  }

  pretraga(){
    let value = this.PretragaForm.value;
    this.registrationService.pretraga(value)
      .subscribe(res=>{
        this.registrationService.smestajUpdate(res);
        this.router.navigate(['/pretraga-prikaz']);
      });

  }

  // public getPretrazeniSmestaj(){
  //   return this.pretrazeniSmestaj;
  // }

  onSubmit() {
    // if (this.myForm.valid) {
    //   console.log(this.myForm.value);
    // }
  }
}
