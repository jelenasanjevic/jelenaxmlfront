import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ServisiService } from '../servisi/servisi.service';

@Component({
  selector: 'app-vidi-ocene',
  templateUrl: './vidi-ocene.component.html',
  styleUrls: ['./vidi-ocene.component.scss']
})
export class VidiOceneComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
 
  ocena = [];

  displayedColumns = ['ocena','datum','username'];
  dataSource = new MatTableDataSource<any>();
  
  constructor(private _data : ServisiService,private router: Router) { }

  ngOnInit() {
    this._data.sveOcene.subscribe(res => {
      this.ocena = res;
      this.dataSource = new MatTableDataSource<any>(res);
      this.dataSource.sort = this.sort;
    }
  )

  }


}
