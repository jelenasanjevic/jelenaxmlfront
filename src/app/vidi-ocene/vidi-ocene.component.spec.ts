import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VidiOceneComponent } from './vidi-ocene.component';

describe('VidiOceneComponent', () => {
  let component: VidiOceneComponent;
  let fixture: ComponentFixture<VidiOceneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VidiOceneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VidiOceneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
