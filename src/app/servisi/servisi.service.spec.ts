import { TestBed, inject } from '@angular/core/testing';

import { ServisiService } from './servisi.service';

describe('ServisiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServisiService]
    });
  });

  it('should be created', inject([ServisiService], (service: ServisiService) => {
    expect(service).toBeTruthy();
  }));
});
