import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ServisiService {
 
  private baseUrl = "http://localhost:8080";

  private sviPretrazeniSmestaji = new BehaviorSubject<any>([]);
  pretrazeniSmestaj = this.sviPretrazeniSmestaji.asObservable();

  private vidiSveKomentare = new BehaviorSubject<any>([]);
  sviKomentari = this.vidiSveKomentare.asObservable();

  private vidiSveOcene = new BehaviorSubject<any>([]);
  sveOcene = this.vidiSveOcene.asObservable();

  private svePoruke = new BehaviorSubject<any>([]);
  porukePoruke = this.svePoruke.asObservable();

  private sveSlike = new BehaviorSubject<any>([]);
  slikeSlike = this.sveSlike.asObservable();


  constructor(private http : Http,private httpDobar: HttpClient) { }



  pretraga(smestaj:any) {
    if(smestaj.tipSmestaja || smestaj.kategorijaSmestaja || smestaj.dorucak || smestaj.mini_kuhinja || smestaj.parking || smestaj.polupansion || smestaj.tv || smestaj.privatno_kupatilo || smestaj.wifi){
      smestaj.detaljna_pretraga = true;
    } else {
      smestaj.detaljna_pretraga = false;
    }
    return this.httpDobar.post(this.baseUrl + "/pretraga", smestaj);
  }
  
  smestajUpdate(smestaj){
    this.sviPretrazeniSmestaji.next(smestaj);
  }

  porukeUpdate(poruke){
    this.svePoruke.next(poruke);
  }

  komentariUpdate(komentar){
    this.vidiSveKomentare.next(komentar);
  }
  oceneUpdate(ocena){
    this.vidiSveOcene.next(ocena);
  }

  slikeUpdate(slike){
    this.sveSlike.next(slike);
  }

  getAllPoruke() : Observable<any> {
    
    return this.httpDobar.get(this.baseUrl + "/getAllPoruke");
  }

  rezervacija(smestajId){
    
    return this.httpDobar.get(this.baseUrl + "/rezervisi/"+smestajId);
  }

  vidiOcene(smestajId){
    
    return this.httpDobar.get(this.baseUrl + "/vidiOcene/"+smestajId);
  }
  
  vidiKomentare(smestajId){
    
    return this.httpDobar.get(this.baseUrl + "/vidiKomentare/"+smestajId);
  }

  getSlikeForSmestaj(smestajId){
    return this.httpDobar.get(this.baseUrl + "/getSlikeForSmestaj/"+smestajId);
  }

  getAllRezervacije() : Observable<any>  {
    
    return this.httpDobar.get(this.baseUrl + "/getAllRezervacijeJ");
  }
  getActiveUser() : any {
    
    return this.httpDobar.get(this.baseUrl + "/getActiveUserJ");
  }

   getPrimalac(id) : any {
    
    return this.httpDobar.get(this.baseUrl + "/getPrimalac/"+id);
  }
  getPrimalacNovi(id) : any {
    
    return this.httpDobar.get(this.baseUrl + "/getPrimalacNovi/"+id);
  }
  odgovoriNaPoruku(poruka:any){
    
    return this.httpDobar.post(this.baseUrl + "/odgovori", poruka);
  }
  odgovoriNaPorukuJ(poruka:any){
    
    return this.httpDobar.post(this.baseUrl + "/odgovoriJ", poruka);
  }


  otkaziRezervaciju(id)  {
    
    return this.httpDobar.get(this.baseUrl + "/otkaziRezervaciju/"+id);
  }
  novaPoruka(id)  {
    
    return this.httpDobar.get(this.baseUrl + "/novaPoruka/"+id);
  }

  
}
