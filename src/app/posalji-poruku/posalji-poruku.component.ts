import { Component, OnInit , ViewChild} from '@angular/core';
import {ServisiService} from '../servisi/servisi.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { resolveDefinition } from '@angular/core/src/view/util';
import { Router } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-posalji-poruku',
  templateUrl: './posalji-poruku.component.html',
  styleUrls: ['./posalji-poruku.component.scss']
})
export class PosaljiPorukuComponent implements OnInit {

  constructor(private _data : ServisiService,private router: Router,private route: ActivatedRoute) { }
  odgovoriForm:any;
  korisnik:any;
  id :any;
  prim:any;
  poruka:any;
  ngOnInit() {
    
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));

    this._data.getActiveUser().subscribe(data=>{
     console.log(data);
      this.korisnik = data;
      
    });

    this._data.getPrimalac(this.id).subscribe(data=>{
     console.log(data);
      this.prim = data;
      
    });

     this.odgovoriForm = new FormGroup({
      primalac:new FormControl(''),
      posaljilac:new FormControl(''),
      sadrzaj: new FormControl('',[Validators.required])
    });

  }

  odgovori(){
    this.odgovoriForm.value.posaljilac = this.korisnik;
    console.log(this.prim);
    this.odgovoriForm.value.primalac = this.prim;
    console.log(this.odgovoriForm.value);
      
      this._data.odgovoriNaPoruku(this.odgovoriForm.value).subscribe(data=>{
        alert('odgovorio');
        this.router.navigate(['/poruke']);
      });
  }
}
