import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosaljiPorukuComponent } from './posalji-poruku.component';

describe('PosaljiPorukuComponent', () => {
  let component: PosaljiPorukuComponent;
  let fixture: ComponentFixture<PosaljiPorukuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosaljiPorukuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosaljiPorukuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
