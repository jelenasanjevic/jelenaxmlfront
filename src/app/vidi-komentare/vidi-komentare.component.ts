import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ServisiService } from 'src/app/servisi/servisi.service';

@Component({
  selector: 'app-vidi-komentare',
  templateUrl: './vidi-komentare.component.html',
  styleUrls: ['./vidi-komentare.component.scss']
})
export class VidiKomentareComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
 
  komentar = [];

  displayedColumns = ['sadrzaj','datum','username'];
  dataSource = new MatTableDataSource<any>();
  
  constructor(private _data : ServisiService,private router: Router) { }

  ngOnInit() {
    this._data.sviKomentari.subscribe(res => {
      this.komentar = res;
      this.dataSource = new MatTableDataSource<any>(res);
      this.dataSource.sort = this.sort;
    }
  )

  }

}
