import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PretragaComponent} from './pretraga/pretraga.component';
import { PregledComponent } from './pregled/pregled.component';
import { PretragaPrikazComponent } from './pretraga-prikaz/pretraga-prikaz.component';
import {PorukeComponent} from './poruke/poruke.component';
import { VidiKomentareComponent } from './vidi-komentare/vidi-komentare.component';
import { VidiOceneComponent } from './vidi-ocene/vidi-ocene.component';
import {SlikeComponent} from './slike/slike.component';
import {ProfilComponent} from './profil/profil.component';
import {PosaljiPorukuComponent} from './posalji-poruku/posalji-poruku.component';

import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from './modules/material/material.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TokenInterceptor } from './services/token.interceptor';
import { EmailDialogComponent } from './components/email-dialog/email-dialog.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { NovaPorukaComponent } from 'src/app/nova-poruka/nova-poruka.component';


const routes: Routes = [
  { path: '', redirectTo:'/login', pathMatch:'full'},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent, pathMatch: 'full' },
  { path: 'reset-password', component: ResetPasswordComponent},
  { path: 'change-password', component: ChangePasswordComponent},
  { path: 'home', component: HomeComponent},
  {
    path:'pretraga',
    component: PretragaComponent
  },
  {
    path:'pregled',
    component: PregledComponent
  },
  {
    path:'pretraga-prikaz',
    component: PretragaPrikazComponent
  },
  {
    path:'poruke',
    component: PorukeComponent
  },
  {
    path:'vidiKomentare',
    component: VidiKomentareComponent
  },
  {
    path:'vidiOcene',
    component: VidiOceneComponent
  },
  {
    path:'slike/:id',
    component: SlikeComponent
  },
  {
    path:'profil',
    component: ProfilComponent
  },
  {
    path:'posalji-poruku/:id',
    component: PosaljiPorukuComponent
  },
  {
    path:'nova-poruka/:id',
    component: NovaPorukaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
