import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ServisiService } from '../servisi/servisi.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  constructor(public _data : ServisiService,private router: Router,private route: ActivatedRoute) { }


  rezervacija:any;
idA :any;
primalac:any;

  displayedColumns = ['id','od','doo','smestajid','izaberi'];
  dataSource = new MatTableDataSource<any>();

  ngOnInit(): void {
   
    this.idA = parseInt(this.route.snapshot.paramMap.get('smestajid.idkorisnika'));
    console.log(this.idA);
    this._data.getAllRezervacije().subscribe(
      data => {
         this.dataSource = new MatTableDataSource<any>(data);
        
        this.dataSource.sort = this.sort;
      }
    )

}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  otkaziRezervaciju(id) {
    this._data.otkaziRezervaciju(id)
    .subscribe(data=>{
      console.log(data);
      alert("otkazana rezervacija!");
      location.reload();

  })
  }

  posaljiPoruku(idkorisnika) {
    console.log(idkorisnika);
    this._data.novaPoruka(idkorisnika).subscribe(data =>
    {
      this.primalac = data;
      console.log(data);
      this.router.navigate(['/nova-poruka',idkorisnika]);
    })
   
  }

}
