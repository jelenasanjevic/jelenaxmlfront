import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
import {ServisiService} from '../servisi/servisi.service';

@Component({
  selector: 'app-poruke',
  templateUrl: './poruke.component.html',
  styleUrls: ['./poruke.component.scss']
})
export class PorukeComponent implements OnInit {

  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private http: HttpClient, private registrationService : ServisiService,private router: Router) { }

  ime: string;
  prezime: string;
  id: any;
  poruka:any;
  korisnik:any;

  displayedColumns = [ 'id', 'sadrzaj','datum','izaberi'];
  dataSource = new MatTableDataSource<any>();

  ngOnInit(): void {
 

      this.registrationService.getAllPoruke().subscribe(
        data => {
           this.dataSource = new MatTableDataSource<any>(data);
          console.log(data);
          this.dataSource.sort = this.sort;
        }
      )

  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
   
  }

  odgovori(id){
    console.log('poruka:');
    console.log(id);
     this.router.navigate(['/posalji-poruku', id]);

}

}
